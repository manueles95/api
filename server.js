//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(function(req, res, next){
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, COntent-Type, Accept");
  next();
})

var requestJson = require('request-json');

var path = require('path');

var urlMlabRaiz = "https://api.mlab.com/api/1/databases/jescobar/collections";
var apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clientesMLabRaiz;

var urlCLientesMongo = "https://api.mlab.com/api/1/databases/jescobar/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

var clientesMLab  = requestJson.createClient(urlCLientesMongo);

app.listen(port);

var movimientosJSONv2 = require('./movimientosv2.json')

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function(req, res) {
  // res.send("Hola Mundo desde Node.js")
  res.sendFile(path.join(__dirname, 'index.html'))
})

app.post('/', function(req, res){
  res.send("Hemos recibido su peticion POST")
})

app.put('/', function(req, res){
  res.send("Hemos recibido su peticion PUT")
})

app.delete('/', function(req, res){
  res.send("Hemos recibido su peticion DELETE")
})

app.get('/clientes', function(req, res){
  res.send("Aqui tiene a los clientes")
  // res.sendFile(path.join(__dirname, 'movimientosv1.json'))
})

app.get('/clientes/:idCliente', function(req, res){
  res.send("Aqui tiene al cliente " + req.params.idCliente);
})

app.get('/v1/movimientos', function(req, res){
  res.sendfile('movimientosv1.json')
})

app.get('/v2/movimientos', function(req, res){
  res.json(movimientosJSONv2)
})

app.get('/v2/movimientos/:id', function(req, res){
  console.log(req.params.id)
  res.send(movimientosJSONv2[req.params.id - 1])
  // res.json(movimientosJSONv2)
})

//query
app.get('/v2/movimientosq', function(req, res){
  console.log(req.query)
  res.send("Peticion con query recibida y cambiada" + req.query )
  // res.json(movimientosJSONv2)
})

app.get('/v2/movimientos/:id/:nombre', function(req, res){
  console.log(req.params.id)
  console.log(req.params.nombre)
  res.send("Recibido")
  // res.json(movimientosJSONv2)
})

//query
app.get('/v2/Clientes', function(req, res){
  clientesMLab.get('', function(err, resM, body){
    if (err){
      console.log(body)
    }else{
      res.send(body);
    }
  })
})

//inserta cliente en mongo db a travez de mlab
app.post('/v2/Clientes', function(req, res){
  clientesMLab.post('', req.body,  function(err, resM, body){
    if (err){
      console.log(body)
    }else{
      res.send(body);
    }
  })
})

//peticion rest para login
app.post('/v2/login', function(req, res){
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, COntent-Type, Accept");
  var email = req.headers.email;
  var password = req.headers.password;
  var query = 'q={"email":"'+ email +'", "password":"'+ password +'"}'
  console.log(query);

  clientesMLabRaiz = requestJson.createClient(urlMlabRaiz + "/Users?" + apiKey + "&" + query);
  console.log(clientesMLabRaiz);
  clientesMLabRaiz.get('', function(err, serM, body){

    if (!err){
      if (body.length > 0){
        res.status(200).send('Usuario logeado correctamente');
      }else{
        res.status(404).send('Usiario no encontrado en el sistema');
      }

    }else{
        console.log(body);
    }

  })
})
